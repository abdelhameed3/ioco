/*global $ */
/*eslint-env browser*/
$(document).ready(function () {
  //header
  $(".main_section .open_list").click(function () {
    $(".list_buttons").addClass("active");
  });
  $(".list_buttons .close_list").click(function () {
    $(".list_buttons").removeClass("active");
  });
  //   $(".main_section .main_content .content button.watch_video").click(
  //     function () {
  //       $(".loader").addClass("active");
  //     }
  //   );

  // Video Player
  var elem = $("#video");
  var configObject = {
    sourceUrl: elem.attr("data-videourl"),
    triggerElement: "#" + elem.attr("id"),
    progressCallback: function () {
      console.log("Callback Invoked.");
    },
  };

  var videoBuild = new YoutubeOverlayModule(configObject);
  videoBuild.activateDeployment();
});
